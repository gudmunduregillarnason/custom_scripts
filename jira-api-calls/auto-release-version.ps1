$USER='gudmundur@curron.is'
$PASSWORD='temporary'
$VERSION_NAME='testVersion'

if (($USER) -and ($PASSWORD) -and ($VERSION_NAME)) {
	'Attempting to create a new released version.'

	$info = @{
		description='Created by Octopus script.'
		name="$VERSION_NAME"
		project='HEIMTJON'
		archived='false'
		released='true'
	}
	$pair = "${USER}:${PASSWORD}"
	$url = 'https://curron.atlassian.net/rest/api/3/version'
	$body = (ConvertTo-Json $info)
	$headers = @{ Authorization = $basicAuthValue }
	$bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
	$base64 = [System.Convert]::ToBase64String($bytes)
	$basicAuthValue = "Basic $base64"
	$headers = @{ Authorization = $basicAuthValue }
	try {
		$newVersion = Invoke-RestMethod -Method 'Post' -Uri $url -Body $body -ContentType 'application/json' -Headers $headers

		'Successfully created new version.'

		# Identical to query used in the 'Create version' UI button in our Kanban board, as of 28.2.2019.
		$queryUrl='https://curron.atlassian.net/rest/api/3/search?jql=project+%3D+HEIMTJON+AND+issuetype+in+%28Bug%2C+Epic%2C+Story%2C+Task%2C+%22Technical+Stuff%22%2C+Sub-task%29+AND+issuetype+%21%3D+%226%22+AND+%28fixVersion+in+unreleasedVersions%28%29+OR+fixVersion+is+EMPTY%29+AND+status+%3D+%2210000%22+AND+fixVersion+is+EMPTY+AND+project+%3D+Heima%C3%BEj%C3%B3nusta+ORDER+BY+Rank+ASC'
		try {
			$listOfIssuesRaw = Invoke-RestMethod -Method 'Get' -Uri $queryUrl -ContentType 'application/json' -Headers $headers
			'Attempting to change issues:'
			$listOfIssuesRaw.issues.key

			$changesToSend = "{`"fields`": {`"fixVersions`": [{`"self`":$(ConvertTo-Json $newVersion.self),`"id`":$(ConvertTo-Json $newVersion.id),`"description`":$(ConvertTo-Json $newVersion.description),`"name`":$(ConvertTo-Json $newVersion.name),`"archived`":$(ConvertTo-Json $newVersion.archived),`"released`":$(ConvertTo-Json $newVersion.released)}]}}"

			Foreach ($issue in $listOfIssuesRaw.issues.key) {
				$issueUrl = "https://curron.atlassian.net/rest/api/3/issue/$issue"
				$issueUrl
				"Attempting to change issue $issue with object: $changesToSend"
				$result = Invoke-RestMethod -Method 'Put' -Uri $issueUrl -Body $changesToSend -ContentType 'application/json' -Headers $headers
				$result
			}
		} catch {
				'Error retrieving list of relevant issues.'
				$_.Exception.GetType().FullName, $_.Exception.Message
				$respstream = $_.Exception.Response.GetResponseStream()
				$sr = new-object System.IO.StreamReader $respstream
				$ErrorResult = $sr.ReadToEnd()
				write-host $ErrorResult
		}
	} catch {
		'Error creating request'
		$_.Exception.GetType().FullName, $_.Exception.Message
	}
} else {
	'Parameters are missing'
}