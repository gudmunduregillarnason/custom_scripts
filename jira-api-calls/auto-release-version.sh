# Command line parameters
USER=$1
PASSWORD=$2
VERSION_NAME=$3

if [[ -z $USER || -z $PASSWORD || -z $VERSION_NAME ]]; # Making sure all command line parameters are met
then
	echo ""	
	echo "Script not run as not all parameteres are met."
	echo "Should be: ./auto-release-version.sh JIRA_USERNAME PASSWORD VERSION_NAME"
else
	echo -e "\nAttempting to create new released version...\n"

	NEW_VERSION_OBJECT_RAW=`curl -H "Content-Type: application/json" -X POST -d "{\"description\": \"Autocreated by 'jira-api-calls.sh' in Octopus.\",\"name\": \"$VERSION_NAME\",\"project\": \"HEIMTJON\",\"archived\": false,\"released\": true}" -u $USER:$PASSWORD https://curron.atlassian.net/rest/api/3/version` # Note this object is identical to the one needed for issue, except it does not contain the releaseDate, which is always day of script run

	IS_ERROR_FREE=`echo -e "$NEW_VERSION_OBJECT_RAW" | jq -r '.errors'`
	echo "$IS_ERROR_FREE"

	if [[ $IS_ERROR_FREE == "null" ]]; # Check if any errors
	then
		echo "Successfully created new version."
		echo "$NEW_VERSION_OBJECT_RAW"
		# An object of the fields to change
		CHANGE_JSON="{\"fixVersions\":[$NEW_VERSION_OBJECT_RAW]}"

		echo -e "\nGetting relevant issue keys...\n"

		# Get the raw json containing the relevant keys

		QUERY_URL='https://curron.atlassian.net/rest/api/3/search?jql=project+%3D+HEIMTJON+AND+issuetype+in+%28Bug%2C+Epic%2C+Story%2C+Task%2C+%22Technical+Stuff%22%2C+Sub-task%29+AND+issuetype+%21%3D+%226%22+AND+%28fixVersion+in+unreleasedVersions%28%29+OR+fixVersion+is+EMPTY%29+AND+status+%3D+%2210000%22+AND+fixVersion+is+EMPTY+AND+project+%3D+Heima%C3%BEj%C3%B3nusta+ORDER+BY+Rank+ASC' # Identical to query used in the 'Create version' UI button in our Kanban board, as of 28.2.2019.
		
		LIST_OF_ISSUES_RAW=$(curl -H "Content-Type: application/json" -X GET -u $USER:$PASSWORD $QUERY_URL)

		# Get the relevant keys from the json string and store as string
		LIST_OF_ISSUES_TO_CHANGE="$(echo $LIST_OF_ISSUES_RAW | jq -r '.issues[].key')" # jq is available at https://stedolan.github.io/jq/download/

		# Create an array from string containing the relevant keys
		readarray -t ARRAY_OF_ISSUES_TO_CHANGE <<< `echo "$LIST_OF_ISSUES_TO_CHANGE"`

		echo -e "\nSetting newly created released version on relevant issues...\n"

		# For every issue in array of keys, set it to the newly created version
		for ISSUE in "${ARRAY_OF_ISSUES_TO_CHANGE[@]}"
		do
			RESULT=$(curl -i -H "Content-Type: application/json" -X PUT -d "{\"fields\": $CHANGE_JSON}" -u $USER:$PASSWORD https://curron.atlassian.net/rest/api/3/issue/$ISSUE)
			echo $RESULT
		done
	else
		printf "\nScript unwilling to proceed due to error:\n"
		echo "$IS_ERROR_FREE"
	fi
fi
